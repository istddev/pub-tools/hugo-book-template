---
type: docs
---

# Documentatie HUGO Book Template

Documentatie hoe standaard [HUGO Book Theme](https://themes.gohugo.io/hugo-book/) via dit GitLab Template te gebruiken.

## [Weergave van diagrammen](./docs/diagrammen)
