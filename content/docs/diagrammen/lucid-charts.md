---
title: "Lucid Charts"
weight: 20
---

## Lucid diagrammen weergeven en bijwerken

Lucid Chart is een online service waarmee diagrammen kunnen worden gemaakt. Er is een mogelijkheid om deze via een link in te sluiten.

{{< bgyellow >}}Een *markdown* content voorbeeld voor het insluiten van een Lucid diagram{{< /bgyellow >}}
Hierbij is de link naar de Lucid Chart https://lucid.app/lucidchart/50c27d27-a23a-4cec-92c2-24a9c84e1d80 en moet de parametercode na **../lucidchart/** in de **Lucid Shortcode** worden meegegeven.

>>{{< inlinetext "{{< lucid 50c27d27-a23a-4cec-92c2-24a9c84e1d80 >}}" >}} 

{{< bgyellow >}}De *publicatie-weergave* van bovenstaande voorbeeld{{< /bgyellow >}}
Daarmee tevens de mogelijkheid om de online Lucid Charts diagram-viewer en/of -editor te gebruiken.

{{< lucid 50c27d27-a23a-4cec-92c2-24a9c84e1d80 >}}