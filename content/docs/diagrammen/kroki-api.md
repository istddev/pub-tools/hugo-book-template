---
title: "Kroki Api"
weight: 10
---

## Diagrammen weergeven via KROKI API

Via de [KROKI API](https://kroki.io/) kunnen *diagrammen* vanuit *tekst-codering* worden weergegeven. In een HUGO Book publicatie kan een *Kroki Shortcode* worden gebruikt om een *Kroki-diagram* binnen *markdown* te coderen. Zie onderstaande voorbeeld.

{{< bgyellow >}}Voorbeeld van een *tekst-codering* voor een *PlantUML-diagram* weergave{{< /bgyellow >}}
  
>>
>>{{< inlinetext "{{< kroki >}}" >}}  
>>{{< inlinetext "```plantuml" >}}  
>>@startuml  
>>"HUGO Book" -> "KROKI Shortcode" :  tekst-codering  
>>"KROKI Shortcode" -> "KROKI API" : opvragen weergave  
>>"KROKI Shortcode" <- "KROKI API": ontvangen weergave  
>>"HUGO Book" <- "KROKI Shortcode": diagram weergave  
>>@enduml  
>>{{< inlinetext "```" >}}  
>>{{< inlinetext "{{< /kroki >}}" >}}  
>>

{{< bgyellow >}}De *publicatie-weergave* van bovenstaande voorbeeld{{< /bgyellow >}}
  
{{< kroki >}}
```plantuml
@startuml
"HUGO Book" -> "KROKI Shortcode" :  tekst-codering
"KROKI Shortcode" -> "KROKI API" : opvragen weergave
"KROKI Shortcode" <- "KROKI API": ontvangen weergave
"HUGO Book" <- "KROKI Shortcode": diagram weergave
@enduml
```
{{< /kroki >}}
