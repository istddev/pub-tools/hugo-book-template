---
title: "Diagrammen"
weight: 1
---

## Diagrammen weergeven

Deze sectie beschrijft de manier waarop diagrammen binnen een HUGO Book publicatie kunnen worden weergegeven. Om dat te kunnen realiseren wordt gebruik gemaakt van **HUGO Shortcode templates** die binnen de *markdown content* kunnen worden gebruikt.

Er zijn momenteel in deze verkenning de volgende initiele *Shortcodes* voor de weergave van diagrammen samengesteld:

- [Kroki Api](kroki-api)

- [lucid Charts](lucid-charts)
